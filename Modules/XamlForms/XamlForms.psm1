Set-StrictMode -Version 2.0

Add-Type -AssemblyName PresentationFramework
Add-Type -AssemblyName System.Windows.Forms


function New-Form {
    [CmdletBinding()]
    [OutputType([psobject])]
    param (
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]
        $XamlPath
    )

    process {
        # XAML
        [xml] $xaml = Get-Content $xamlPath
        $xaml.Window.RemoveAttribute('x:Class')
        $xaml.Window.RemoveAttribute('mc:Ignorable')
        $xaml.Window.RemoveAttribute('xmlns:local')

        $namespaceMgr = New-Object System.Xml.XmlNamespaceManager $xaml.NameTable
        $namespaceMgr.AddNamespace('x', 'http://schemas.microsoft.com/winfx/2006/xaml')

        $xamlReader = New-Object System.Xml.XmlNodeReader $xaml

        $window = New-Object -TypeName psobject
        Add-Member -InputObject $window -MemberType NoteProperty -Name "Self" -Value ([Windows.Markup.XamlReader]::Load($xamlReader))

        # UI Elements
        $xaml.SelectNodes('//*[@x:Name]', $namespaceMgr) | ForEach-Object {
            Add-Member -InputObject $window -MemberType NoteProperty ($_.Name) -Value $window.Self.FindName($_.Name)
        }

        return $window
    }
}


Export-ModuleMember -Function New-Form
