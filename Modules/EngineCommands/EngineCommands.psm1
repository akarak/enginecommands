﻿Set-StrictMode -Version 2.0

function Send-EngineCommands {
    [CmdletBinding()]
    [OutputType([string])]
    param (
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]
        $RemoteHost = "localhost",

        [Parameter()]
        [ValidateRange(1, 65535)]
        [uint16]
        $RemotePort = 6100,

        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [ValidateNotNullOrEmpty()]
        [Alias("Command")]
        [string[]]
        $Commands,

        [Parameter()]
        [switch]
        $NeedResponse = $false,

        [Parameter()]
        [ValidateRange(1, 65535)]
        [uint16]
        $ConnectTimeout = 500,

        [Parameter()]
        [ValidateRange(1, 65535)]
        [uint16]
        $ReadTimeout = 5000
    )

    begin {
        $socket = New-Object -TypeName Net.Sockets.TcpClient

        Write-Verbose -Message ("Establishing connection to Viz Engine at {0}:{1}." -f $RemoteHost, $RemotePort)

        try {
            $iar = $socket.BeginConnect($RemoteHost, $RemotePort, $null, $null)
            if (!$iar.AsyncWaitHandle.WaitOne($ConnectTimeout))
            {
                Write-Verbose -Message "Connection timeout."
                return Format-EngineResponse -Response "Connection timeout"
            }
        }
        catch [System.Net.Sockets.SocketException] {
            Write-Verbose -Message ("I/O Exception. {0}" -f $_)
            return Format-EngineResponse -Response ("Connection wasn't established. {0}" -f $_)
        }
        finally {
            if ($socket.Connected) {
                if ($iar) {
                    [void] $socket.EndConnect($iar)
                }
            }
        }

        Write-Verbose -Message "Connection established."

        $encoding = New-Object -TypeName System.Text.UTF8Encoding -ArgumentList $false

        $stream = $socket.GetStream()
        $stream.ReadTimeout = $ReadTimeout
    }

    process {
        if ($socket.Connected) {
            try {
                foreach ($index in 1..$Commands.Length) {
                    $command = $Commands[$index - 1]
    
                    if ($PSBoundParameters['Verbose']) {
                        $local:messageLength = 30
                        Write-Verbose -Message ("Sending command [{0}]." -f (&{if ($command.Length -gt $messageLength) { $command.SubString(0, $messageLength) + "..."} else { $command }}))
                    }
    
                    $commandMessage = "{0} {1}`0" -f (&{if ($NeedResponse) { $index } else {"-1"}}), $command
                    $commandBuffer = $encoding.GetBytes($commandMessage)
    
                    $stream.Write($commandBuffer, 0, $commandBuffer.Length)
                    $stream.Flush()
    
                    if ($NeedResponse) {
                        $responseMessage = New-Object -TypeName System.Text.StringBuilder -ArgumentList 1024
                        $responseBuffer = New-Object -TypeName System.Byte[] -ArgumentList $socket.ReceiveBufferSize
    
                        Write-Verbose -Message ("Reading response (timeout {0} ms, buffer size {1} byte(s)):" -f $stream.ReadTimeout, $socket.ReceiveBufferSize)
                        [uint16] $blockIndex = 1
    
                        do {
                            try {
                                [long] $dataSize = $stream.Read($responseBuffer, 0, $responseBuffer.Length)
                                [void] $responseMessage.Append($encoding.GetString($responseBuffer, 0, $dataSize))
                                Write-Verbose -Message ("`tblock #{0} - {1} byte(s)" -f $blockIndex, $dataSize)
                                $blockIndex += 1
                            }
                            catch [System.IO.IOException] {
                                Write-Verbose -Message ("I/O Exception. {0}" -f $_)
                                if (!$stream.DataAvailable) {
                                    return Format-EngineResponse $_
                                }
                            }
                        } while ($stream.DataAvailable -and $socket.Client.Connected)
    
                        if ($responseMessage.Length -gt 0) {
                            Write-Verbose -Message 'Parsing response.'
                            [string] $private:message = $responseMessage.ToString().TrimEnd("`0").SubString(2)
                            if ($private:message -match 'ERROR\s\<(?:.*)\>\s:\s(.*)') {
                                $private:message = (Get-Culture).TextInfo.ToTitleCase($Matches[1].TrimEnd())
                                Write-Verbose -Message ("Viz Engine Error. {0}" -f $private:message)
                                Format-EngineResponse -Response $private:message | Write-Output
                            } else {
                                Format-EngineResponse -Pass -Response $private:message | Write-Output
                            }
                        } else {
                            Format-EngineResponse -Pass | Write-Output
                        }
                    }
                }
            }
            catch [System.Net.Sockets.SocketException] {
                Write-Verbose -Message ("I/O Exception. {0}" -f $_)
                return Format-EngineResponse $_
            }
        }
    }

    end {
        if ($socket.Connected) {
            if ($stream) {
                [void] $stream.Close()
            }

            Write-Verbose -Message ("Disconnectiong from Viz Engine at {0}:{1}." -f $RemoteHost, $RemotePort)
        }

        [void] $socket.Close()
    }
}


function Format-EngineResponse {
    [CmdletBinding()]
    [OutputType([PSCustomObject])]
    param (
        [Parameter()]
        [switch]
        $Pass = $false,

        [Parameter(ValueFromPipeline=$true)]
        [string]
        $Response = ""
    )
    process {
        return [PSCustomObject] @{
            Pass = $Pass
            Response = $Response
        }
    }
}


Export-ModuleMember -Function Send-EngineCommands
