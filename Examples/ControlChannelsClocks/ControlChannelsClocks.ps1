﻿#Requires -Version 3.0

$env:PSModulePath = "$PSScriptRoot\..\..\Modules;$PSScriptRoot\Modules;$env:PSModulePath"
Import-Module EngineCommands.psm1 -Force
Import-Module XamlForms.psm1 -Force


$Verbose = $true
$ScriptBaseName = (Get-Item $PSCommandPath).BaseName
$ScriptPathWithBaseName = Join-Path $PSScriptRoot $ScriptBaseName


try {
    $Config = Get-Content -Path ($ScriptPathWithBaseName + ".Config.json") -encoding UTF8 -Raw -ErrorAction Stop | ConvertFrom-Json
    $Channels = @($Config.Channels | ForEach-Object {
            @{
                Description = $_.Description
                Host = $_.Host
                Port = $_.Port
                Params = $_.Params
                Status = "OK"
            }
        } | Sort-Object -Property @{e = {$_.Description}}
    )
}
catch {
    [System.Windows.Forms.MessageBox]::Show("При загрузке файла конфигурации произошла ошибка.",
        "Ошибка",
        [System.Windows.Forms.MessageBoxButtons]::OK,
        [System.Windows.Forms.MessageBoxIcon]::Hand)
}

$Window = New-Form -XamlPath ($ScriptPathWithBaseName + ".Window.xaml")


<#
    Functions
#>


function Get-LogFilename {
    [OutputType([string])]
    param (
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]
        $Path,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]
        $Filename
    )

    [string] $logPath = [System.Environment]::ExpandEnvironmentVariables($Path)
    if (!$logPath) {
        $logPath = $Env:Temp
    }

    return (Join-Path -Path $logPath -ChildPath $Filename)
}


function Write-Log {
    [CmdletBinding()]
    param(
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string] $Message,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [ValidateSet('Information', 'Warning', 'Error')]
        [string] $Severity = 'Information'
    )

    [pscustomobject] @{
        Time = (Get-Date -Format g)
        Message = $Message
        Severity = $Severity
    } | Export-Csv -Path (Get-LogFilename $Config.LogPath "$ScriptBaseName.log") -Encoding UTF8 -NoTypeInformation -Append -Force
}


function Invoke-LoadSceneScript([string] $Scene) {
    foreach ($channel in $Channels) {
        $commands = @(
            ("SCENE*{0} LOAD" -f $Scene),
            ("SCENE*{0}*STAGE*DIRECTOR*Default SHOW `$pilot1" -f $Scene),
            ("RENDERER SET_OBJECT SCENE*{0}" -f $Scene),
            ("RENDERER*TREE*`$object*FUNCTION*ControlObject*in SET {0}" -f $channel.Params)
        )

        $channel.Status = "OK"
        Send-EngineCommands -RemoteHost $channel.Host -RemotePort $channel.Port -Commands $commands -NeedResponse -ReadTimeout 10000 -Verbose:$Verbose | ForEach-Object {
            if (!$_.Pass) {
                $channel.Status = $_.Response
            }
        }
    }
}


function Invoke-ShowClockScript {
    $commands = @(
        'RENDERER*STAGE*DIRECTOR*Default START'
    )

    foreach ($channel in $Channels) {
        $channel.Status = "OK"
        Send-EngineCommands -RemoteHost $channel.Host -RemotePort $channel.Port -Commands $commands -NeedResponse -Verbose:$Verbose | ForEach-Object {
            if (!$_.Pass) {
                $channel.Status = $_.Response
            }
        }
    }
}


function Invoke-HideClockScript {
    $commands = @(
        'RENDERER*STAGE*DIRECTOR*Default CONTINUE'
    )

    foreach ($channel in $Channels) {
        $channel.Status = "OK"
        Send-EngineCommands -RemoteHost $channel.Host -RemotePort $channel.Port -Commands $commands -NeedResponse -Verbose:$Verbose | ForEach-Object {
            if (!$_.Pass) {
                $channel.Status = $_.Response
            }
        }
    }
}


function Invoke-CleanupChannelsScript([string[]] $Scenes) {
    $commands = @(
        'RENDERER SET_OBJECT'
    )

    foreach ($channel in $Channels) {
        Send-EngineCommands -RemoteHost $channel.Host -RemotePort $channel.Port -Commands $commands -NeedResponse -Verbose:$Verbose | ForEach-Object {
            if (!$_.Pass) {
                $channel.Status = $_.Response
            }
        }
    }

    $commands = @(
        $Scenes | ForEach-Object { "SCENE*{0} CLOSE" -f $_}
    )

    foreach ($channel in $Channels) {
        Send-EngineCommands -RemoteHost $channel.Host -RemotePort $channel.Port -Commands $commands -NeedResponse -Verbose:$Verbose | ForEach-Object {
            $channel.Status = "OK"
        }
    }
}


<#
    Initialization
#>


$Window | Add-Member -MemberType ScriptMethod -Name "Update_Status" -Value {
    param(
        [string]
        $Message
    )

    $this.sbiStatus.Content = $Message
    Write-Log -Message $Message -Severity Information
}


$Window | Add-Member -MemberType ScriptMethod -Name "Update_Channels" -Value {
    param(
        [PSCustomObject[]]
        $Source
    )

    $this.lvChannels.ItemsSource = $Source
}


$Window.Self.Add_Closing({
    $_.Cancel = ([System.Windows.Forms.MessageBox]::Show("Завершить работу приложения?",
        $Window.Self.Title, [System.Windows.Forms.MessageBoxButtons]::YesNo,
        [System.Windows.Forms.MessageBoxIcon]::Question,
        [System.Windows.Forms.MessageBoxDefaultButton]::Button2) -ne 'Yes')
})


$Window.btnCleanup.Add_Click({
    Invoke-CleanupChannelsScript(@($Config.Scenes.AEK, $Config.Scenes.DUT))
    $Window.Update_Channels($Channels)
    $Window.Update_Status("Выполнена команда очистки каналов")
})


$Window.btnDUT_ShowClock.Add_Click({
    Invoke-ShowClockScript
    $Window.Update_Channels($Channels)
    $Window.Update_Status("Выполнена команда показа часов")
})


$Window.btnDUT_HideClock.Add_Click({
    Invoke-HideClockScript
    $Window.Update_Channels($Channels)
    $Window.Update_Status('Выполнена команда уборки часов')
})


$Window.btnDUT_LoadScene.Add_Click({
    Invoke-LoadSceneScript($Config.Scenes.DUT)
    $Window.Update_Channels($Channels)
    $Window.Update_Status('Выполнена загрузка сцены с обычными часами ДУТ')
})



$Window.btnAEK_Std_ShowClock.Add_Click({
    Invoke-ShowClockScript
    $Window.Update_Channels($Channels)
    $Window.Update_Status('Выполнена команда показа часов')
})


$Window.btnAEK_Std_HideClock.Add_Click({
    Invoke-HideClockScript
    $Window.Update_Channels($Channels)
    $Window.Update_Status('Выполнена команда уборки часов')
})


$Window.btnAEK_Std_LoadScene.Add_Click({
    Invoke-LoadSceneScript($config.Scenes.AEK_Std)
    $Window.Update_Channels($Channels)
    $Window.Update_Status('Выполнена загрузка сцены с обычными часами АЭК')
})


$Window.btnAEK_7N19_ShowClock.Add_Click({
    Invoke-ShowClockScript
    $Window.Update_Channels($Channels)
    $Window.Update_Status('Выполнена команда показа часов')
})


$Window.btnAEK_7N19_HideClock.Add_Click({
    Invoke-HideClockScript
    $Window.Update_Channels($Channels)
    $Window.Update_Status('Выполнена команда уборки часов')
})


$Window.btnAEK_7N19_LoadScene.Add_Click({
    Invoke-LoadSceneScript($config.Scenes.AEK_7N19)
    $Window.Update_Channels($Channels)
    $Window.Update_Status('Выполнена загрузка сцены с часами "7 ноября 2019"')
})


$Window.Update_Status("Файл конфигурации успешно загружен")
$Window.Update_Channels($Channels)


[void] $Window.Self.ShowDialog()
