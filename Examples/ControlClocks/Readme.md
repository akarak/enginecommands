### Требования

Для работы приложения требуется PowerShell версии 3.0 и выше. Версию 3.0 можно загрузить по ссылке https://www.microsoft.com/en-us/download/details.aspx?id=34595.

### Запуск

Перед первым запуском приложения необходимо разрешить выполнение неподписанных скриптов PowerShell с помощью команды `powershell Set-ExecutionPolicy RemoteSigned`.

Для упрощения запуска приложения можно создать для него shortcut с командой `powershell -NoProfile -WindowStyle Hidden -File "<полный путь к файлу ControlChannelsClocks.ps1>"`.
