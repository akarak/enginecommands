$env:PSModulePath = "$PSScriptRoot\..\..\Modules;$PSScriptRoot\Modules;$env:PSModulePath"
Import-Module EngineCommands.psm1 -Force

$remoteHost = "127.0.0.1"
$remotePort = 6100
$verbose = $true

Write-Host "Test #1" -ForegroundColor Green

Send-EngineCommands $remoteHost $remotePort "MAIN VERSION" -NeedResponse -Verbose:$verbose | Select-Object -ExpandProperty Response | Write-Host

Write-Host "Test #2" -ForegroundColor Green

Send-EngineCommands -Commands "MAIN VERSION" -NeedResponse -Verbose:$verbose | Select-Object -ExpandProperty Response | Write-Host
$response = (Send-EngineCommands -Commands "REND GET_OBJECT" -NeedResponse -Verbose:$verbose | Select-Object -ExpandProperty Response)
if ($response) {
    Send-EngineCommands -Commands ($response + "*NAME GET") -NeedResponse -Verbose:$verbose | Select-Object -ExpandProperty Response | Write-Host
}

Write-Host "Test #3" -ForegroundColor Green

$commands = @('MAIN VERSION', 'REND GET_OBJECT')
Send-EngineCommands -Commands $commands -NeedResponse -Verbose:$verbose | Select-Object -ExpandProperty Response | Write-Host

Write-Host "Test #4" -ForegroundColor Green

'MAIN VERSION', 'REND GET_OBJECT' | Send-EngineCommands -NeedResponse -Verbose:$verbose | Select-Object -ExpandProperty Response | Write-Host

Write-Host "Test #5" -ForegroundColor Green

'MAIN VERSION', 'FONT GET_DETAIL2 /Resources/Fonts' | Send-EngineCommands $remoteHost $remotePort -NeedResponse -Verbose:$verbose | Select-Object -ExpandProperty Response | Write-Host

# 'peak_get_icon CAPTURE RENDERER FULL_FRAME BGRA -1 -1 RAW' | Send-EngineCommands $remoteHost $remotePort -NeedResponse -Verbose:$verbose | Select-Object -ExpandProperty Response | Write-Host
