# EngineCommands

**EngineCommands** -- это набор модулей для [Windows PowerShell](https://docs.microsoft.com/ru-ru/powershell/ "Windows Powershell"), предназначенный для управления [Viz Engine](https://www.vizrt.com/products/viz-engine "Viz Engine"). Модули могут использоваться как независимые командлеты, так и в составе консольных приложений или приложений с GUI.

![](Docs/Assets/Screencast_01.gif)

## Возможности

* Обмен данными с Viz Engine:
    * Отправка команд и возврат результата их выполнения.
    * Поддержка работы с [конвейером](https://docs.microsoft.com/ru-ru/powershell/scripting/learn/understanding-the-powershell-pipeline "конвейером") команд PowerShell.
    * Поддержка генерации исключений при возникновении ошибок.
* Построение приложений с использованием GUI на Xaml.

## Примеры приложений

### Зависимости

Для работы приложений требуется Windows PowerShell версии 3.0 и выше. Версию 3.0 можно загрузить по ссылке https://www.microsoft.com/en-us/download/details.aspx?id=34595.

### Запуск приложений

Перед первым запуском приложения необходимо разрешить выполнение неподписанных скриптов PowerShell с помощью команды `powershell Set-ExecutionPolicy RemoteSigned`.

Для упрощения запуска приложения можно создать для него shortcut с командой `powershell -NoProfile -WindowStyle Hidden -File "<полный путь к файлу ControlChannelsClocks.ps1>"`.

## Лицензия

Набор модулей **EngineCommands** выпущен под лицензией [MIT](http://www.opensource.org/licenses/MIT "MIT").

------------

Copyright 2019 Alexey Karak
